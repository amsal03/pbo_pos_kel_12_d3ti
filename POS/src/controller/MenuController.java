/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import model.Barang;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class MenuController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TableView<Barang> tabel;
    @FXML
    private TableColumn TBLno;
    @FXML
    private TableColumn TBLnama;
    @FXML
    private TableColumn TBLharga;
    @FXML
    private TextField TFnama;
    @FXML
    private TextField TFharga;

    ObservableList<Barang> tableData = null;
    public int id_toko = LoginController.id_toko;
    public int id_barang = -1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        executeHQLQuery();
    }

    @FXML
    private void createAction(ActionEvent event) {
        if (TFnama.getText().equals("") || TFharga.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else if (this.id_barang != -1) {
            Alert alert = new Alert(Alert.AlertType.NONE, "UnSelect Data", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            executeHQLQueryCreate();
        }
    }

    private void executeHQLQueryCreate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("INSERT INTO Barang (nama,harga,id_toko) VALUES (:nama,:harga,:idToko)");
            q.setParameter("nama", TFnama.getText());
            q.setParameter("harga", TFharga.getText());
            q.setParameter("idToko", id_toko);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void updateAction(ActionEvent event) {
        if (TFharga.getText().equals("") || TFnama.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else if (this.id_barang == -1) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Pilih data yang akan di upadte", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            executeHQLQueryUpdate();
        }
    }

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("Update Barang SET nama = :nama ,harga = :harga WHERE id_barang = :idBarang");
            q.setParameter("nama", TFnama.getText());
            q.setParameter("harga", TFharga.getText());
            q.setParameter("idBarang", this.id_barang);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteAction(ActionEvent event) {
        if (this.id_barang != -1) {
            executeHQLQueryDelete();
        } else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        }

    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("DELETE Barang WHERE id_barang = :idBarang");
            q.setParameter("idBarang", this.id_barang);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/AdminHome.fxml"));
        pane.getChildren().setAll(panes);
    }

    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Barang where id_toko = :idToko");
            q.setParameter("idToko", id_toko);
            List resultList = q.list();
            displayList(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    
    private void displayList(List ResultList) {
        tableData = FXCollections.observableArrayList();
        for (Object o : ResultList) {
            Barang brg = (Barang) o;
            tableData.add(brg);
        }
        TBLno.setCellValueFactory(new PropertyValueFactory<>("idBarang"));
        TBLnama.setCellValueFactory(new PropertyValueFactory<>("nama"));
        TBLharga.setCellValueFactory(new PropertyValueFactory<>("harga"));

        tabel.getColumns().clear();
        tabel.setItems(tableData);
        tabel.getColumns().addAll(TBLno, TBLnama, TBLharga);

        tabel.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                onClick();
            }
            
         

        });
        
      
        
        tabel.setRowFactory(new Callback<TableView<Barang>, TableRow<Barang>>() {

            @Override
            public TableRow<Barang> call(TableView<Barang> param) {
                final TableRow<Barang> row = new TableRow<Barang>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < tabel.getItems().size() && tabel.getSelectionModel().isSelected(index)) {
                            tabel.getSelectionModel().clearSelection();
                            event.consume();
                            offClick();
                        }

                    }

                });
                return row;
            }
            
            
        });
    }

    public void onClick() {
        if (tabel.getSelectionModel().getSelectedItem() != null) {
            Barang forumSelect = tabel.getSelectionModel().getSelectedItem();
            this.id_barang = forumSelect.getIdBarang();
            TFnama.setText(forumSelect.getNama());
            TFharga.setText(forumSelect.getHarga());
        }
    }

    public void offClick() {
        this.id_barang = -1;
        TFnama.setText("");
        TFharga.setText("");
    }
    
    public void looping(){
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }
    
    
}
