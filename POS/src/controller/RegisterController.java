/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.Toko;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class RegisterController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField TFnamaToko;
    @FXML
    private TextField TFusername;
    @FXML
    private PasswordField TFpassword;
    @FXML
    private TextField TFuser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void registerAction(ActionEvent event) {
        if (TFpassword.getText().equals("") || TFnamaToko.getText().equals("") || TFusername.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            executeHQLQuery();
        }
    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
        pane.getChildren().setAll(panes);
    }

    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

//            Toko toko = new Toko();
//            toko.setNamaToko(TFnamaToko.getText());
            Query tokos = session.createSQLQuery("INSERT INTO Toko (nama_toko) VALUES (:nama)");
            tokos.setParameter("nama", TFnamaToko.getText());
            tokos.executeUpdate();

            //session.save(toko);

            session.getTransaction().commit();

            Session sessionTwo = NewHibernateUtil.getSessionFactory().openSession();
            sessionTwo.beginTransaction();
            
            //User user = new User();
            //user.setNama(TFuser.getText());
            //user.setPassword(TFpassword.getText());
            //user.setRole(1);
            //user.setUsername(TFusername.getText());
            //user.setIdToko(toko.getIdToko());
            
            //sessionTwo.save(user);
            BigInteger result =  (BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult();
            
            Query q = sessionTwo.createSQLQuery("INSERT INTO User (username,password,id_toko,nama,role) VALUES (:username,:password,:idToko,:nama,:role)");
            q.setParameter("username", TFusername.getText());
            q.setParameter("password", TFpassword.getText());
            q.setParameter("nama", TFuser.getText());
            q.setParameter("role", 1);
            q.setParameter("idToko", result);
            q.executeUpdate();
            sessionTwo.getTransaction().commit();
            sessionTwo.close();

            pindah();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pindah() throws IOException {

        Alert alert = new Alert(Alert.AlertType.NONE, "Data berhasil di daftarkan", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();

        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
        pane.getChildren().setAll(panes);
    }

}
