/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import model.Barang;
import model.BarangTransaksi;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class BayarController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TableView<BarangTransaksi> tabel;
    @FXML
    private TableColumn TBLno;
    @FXML
    private TableColumn TBLnama;
    @FXML
    private TableColumn tblJumlah;
    @FXML
    private TableColumn tblstotal;
    @FXML
    private Text TxtTotal;
    @FXML
    private TextField TFkalkulator;
    @FXML
    private Text txtKembalian;

    public static int cash;
    public static BigInteger id;
    public static int kembalian;
    ObservableList<BarangTransaksi> tableData = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        init();
        executeHQLQuery();
    }

    public void init() {
        TxtTotal.setText(HomeController.sum.toString());
        this.id = HomeController.result;
    }

    @FXML
    private void hitungAction(ActionEvent event) {
        cash = Integer.parseInt(TFkalkulator.getText());
        kembalian = cash - Integer.parseInt(HomeController.sum.toString());
        txtKembalian.setText(String.valueOf(kembalian));
    }

    @FXML
    private void cetakAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Struck.fxml"));
        pane.getChildren().setAll(panes);
    }

    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from BarangTransaksi WHERE id_transaksi = :idTransaksi");
            q.setParameter("idTransaksi", this.id);
            List resultList = q.list();
            displayList(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayList(List ResultList) {
        tableData = FXCollections.observableArrayList();
        for (Object o : ResultList) {
            BarangTransaksi brg = (BarangTransaksi) o;
            tableData.add(brg);
            //System.out.println(brg);
        }
        TBLno.setCellValueFactory(new PropertyValueFactory<>("idBarangTransaksi"));
        TBLnama.setCellValueFactory(new PropertyValueFactory<>("idBarang"));
        tblJumlah.setCellValueFactory(new PropertyValueFactory<>("jumlah"));
        tblstotal.setCellValueFactory(new PropertyValueFactory<>("subTotal"));

        tabel.getColumns().clear();
        tabel.setItems(tableData);
        tabel.getColumns().addAll(TBLno, TBLnama, tblJumlah, tblstotal);

    }

}
