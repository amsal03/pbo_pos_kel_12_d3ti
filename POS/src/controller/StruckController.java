/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class StruckController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private Text TxtToko;
    @FXML
    private Text tgl;
    @FXML
    private Text total;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        init();
    }

    public StruckController() {
    }

    public void init() {
        TxtToko.setText("Struk Belajaan");
        total.setText(HomeController.sum.toString());

        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("SELECT T.tanggal From Transaksi T WHERE id_transaksi = :idTransaksi");
            q.setParameter("idTransaksi", HomeController.result);
            List resultList = q.list();
            tgl.setText(resultList.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Home.fxml"));
        pane.getChildren().setAll(panes);
    }

}
