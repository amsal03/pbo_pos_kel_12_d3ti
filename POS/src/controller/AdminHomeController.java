/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class AdminHomeController implements Initializable {
    @FXML
    private AnchorPane pane;
    public int id_toko;
    
    public static String nama_toko;
    @FXML
    private Text namaToko;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.id_toko = LoginController.id_toko;
        displayTokoName();
    }    
    
     public void displayTokoName(){
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("SELECT a.namaToko From Toko a where id_toko = :idToko");
            q.setParameter("idToko", id_toko);
            List resultList = q.list();
            for (Object o : resultList) {
                this.nama_toko = (String) o;
            }
            namaToko.setText(nama_toko);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @FXML
    private void pegawaiAction(ActionEvent event) throws IOException {
    AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Pegawai.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    private void transaksiAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/LaporanTransaksi.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    private void logoutAcion(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    private void menuAction(ActionEvent event) throws IOException {
    AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Menu.fxml"));
        pane.getChildren().setAll(panes);
    }
    
}
