/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import model.Barang;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class HomeController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TableView<Barang> tabel;
    @FXML
    private TableColumn TBLno;
    @FXML
    private TableColumn TBLnama;
    @FXML
    private TableColumn TBLharga;
    @FXML
    private TextField jumlah;

    ObservableList<Barang> tableData = null;

    public int id_toko = LoginController.id_toko;
    public int id_barang = -1;
    public int jlh = 0;
    public static BigInteger result;
    public String harga = "0";
    public int subTotal;
    public static BigInteger sum;
    @FXML
    private TextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        executeHQLQuery();
        createTransaksi();
    }

    public void createTransaksi() {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();

            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("INSERT INTO Transaksi (id_toko,tanggal,total) VALUES (:idToko, :tanggal,:total)");
            q.setParameter("tanggal", now.format(dtf));
            q.setParameter("total", 0);
            q.setParameter("idToko", id_toko);
            q.executeUpdate();
            session.getTransaction().commit();
            result = (BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void minusAction(ActionEvent event) {
        if (jlh != 0) {
            jlh--;
            jumlah(jlh);
        }
    }

    private void jumlah(int jlh) {
        jumlah.setText(String.valueOf(jlh));
    }

    @FXML
    private void plusAction(ActionEvent event) {
        if (jlh != 10) {
            jlh++;
            jumlah(jlh);
        }
    }

    @FXML
    private void cartAction(ActionEvent event) {

        subTotal = Integer.parseInt(harga) * jlh;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("INSERT INTO Barang_Transaksi (id_barang,id_transaksi,jumlah,sub_total) VALUES (:idBarang, :idTransaksi, :jumlah, :subTotal)");
            q.setParameter("idBarang", this.id_barang);
            q.setParameter("idTransaksi", this.result);
            q.setParameter("jumlah", this.jlh);
            q.setParameter("subTotal", subTotal);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void bayarAction(ActionEvent event) throws IOException {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("SELECT SUM(sub_total) AS Total FROM Barang_Transaksi WHERE id_transaksi = :idTransaksi");
            q.setParameter("idTransaksi", this.result);
            List resultList = q.list();
            //int hasil = Integer.parseInt(resultList.toString());
            //System.out.println(hasil);
            for (Object o : resultList) {
                this.sum = (BigInteger) o;
                //System.out.println(su);
            }

            session.getTransaction().commit();
            session.close();

            Session sessions = NewHibernateUtil.getSessionFactory().openSession();
            sessions.beginTransaction();
            Query a = sessions.createSQLQuery("Update Transaksi SET total = :total WHERE id_transaksi = :idTransaksi");
            a.setParameter("total", sum);
            a.setParameter("idTransaksi", this.result);
            a.executeUpdate();
            sessions.getTransaction().commit();
            sessions.close();

            AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Bayar.fxml"));
            pane.getChildren().setAll(panes);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Barang where id_toko = :idToko");
            q.setParameter("idToko", id_toko);
            List resultList = q.list();
            displayList(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayList(List ResultList) {
        tableData = FXCollections.observableArrayList();
        for (Object o : ResultList) {
            Barang brg = (Barang) o;
            tableData.add(brg);
        }
        TBLno.setCellValueFactory(new PropertyValueFactory<>("idBarang"));
        TBLnama.setCellValueFactory(new PropertyValueFactory<>("nama"));
        TBLharga.setCellValueFactory(new PropertyValueFactory<>("harga"));

        tabel.getColumns().clear();
        tabel.setItems(tableData);
        tabel.getColumns().addAll(TBLno, TBLnama, TBLharga);

        tabel.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                onClick();
            }

        });
        tabel.setRowFactory(new Callback<TableView<Barang>, TableRow<Barang>>() {

            @Override
            public TableRow<Barang> call(TableView<Barang> param) {
                final TableRow<Barang> row = new TableRow<Barang>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < tabel.getItems().size() && tabel.getSelectionModel().isSelected(index)) {
                            tabel.getSelectionModel().clearSelection();
                            event.consume();
                            offClick();
                        }

                    }

                });
                return row;
            }

        });
    }

    public void onClick() {
        if (tabel.getSelectionModel().getSelectedItem() != null) {
            Barang forumSelect = tabel.getSelectionModel().getSelectedItem();
            this.id_barang = forumSelect.getIdBarang();
            this.harga = forumSelect.getHarga();
        }
    }

    public void offClick() {
        this.id_barang = -1;
        this.jlh = 0;
        this.harga = "0";
        this.subTotal = 0;
        jumlah(jlh);
    }

    @FXML
    private void outAction(ActionEvent event) throws IOException {
        Session sessions = NewHibernateUtil.getSessionFactory().openSession();
        sessions.beginTransaction();
        Query a = sessions.createSQLQuery("Delete Transaksi WHERE id_transaksi = :idTransaksi");
        a.setParameter("idTransaksi", this.result);
        a.executeUpdate();

        Query b = sessions.createSQLQuery("Delete Barang_Transaksi WHERE id_transaksi = :idTransaksi");
        b.setParameter("idTransaksi", this.result);
        b.executeUpdate();

        sessions.getTransaction().commit();
        sessions.close();

        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    private void searchAction(ActionEvent event) {

        if ((search.getText()).equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Field Pertanyaan harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            try {
                Session session = NewHibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();
                Query q = session.createQuery("from Barang where id_toko = :idToko AND nama like '%" + search.getText() +"%'");
                q.setParameter("idToko", id_toko);
                List resultList = q.list();
                displayList(resultList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
