/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class PegawaiController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private TableView<User> tabel;
    @FXML
    private TableColumn TBLno;
    @FXML
    private TableColumn TBLusername;
    @FXML
    private TableColumn TBLpassword;
    @FXML
    private TextField TFnama;
    @FXML
    private TextField TFpassword;

    ObservableList<User> tableData = null;

    public int id_toko = LoginController.id_toko;
    @FXML
    private TableColumn TBLnama;
    @FXML
    private TextField TFusername;
    public int id_pegawai = -1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        executeHQLQuery();
    }

    @FXML
    private void createAction(ActionEvent event) {
        if (TFpassword.getText().equals("") || TFnama.getText().equals("") || TFpassword.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else if (this.id_pegawai != -1) {
            Alert alert = new Alert(Alert.AlertType.NONE, "UnSelect Data", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        }  else {
            executeHQLQueryCreate();
        }
    }

    private void executeHQLQueryCreate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("INSERT INTO User (username,password,id_toko,nama,role) VALUES (:username,:password,:idToko,:nama,:role)");
            q.setParameter("username", TFusername.getText());
            q.setParameter("password", TFpassword.getText());
            q.setParameter("nama", TFnama.getText());
            q.setParameter("role", 2);
            q.setParameter("idToko", id_toko);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void updateAction(ActionEvent event) {
        if (TFpassword.getText().equals("") || TFnama.getText().equals("") || TFpassword.getText().equals("") || this.id_pegawai < 0) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else if(this.id_pegawai == -1){
            Alert alert = new Alert(Alert.AlertType.NONE, "Pilih data yang akan di upadte", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        }else {
            executeHQLQueryUpdate();
        }
    }

    private void executeHQLQueryUpdate() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("Update User SET username = :username ,password = :password,nama = :nama WHERE id_user = :idUser");
            q.setParameter("username", TFusername.getText());
            q.setParameter("password", TFpassword.getText());
            q.setParameter("nama", TFnama.getText());
            q.setParameter("idUser", this.id_pegawai);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteAction(ActionEvent event) {
        if(this.id_pegawai != -1){
            executeHQLQueryDelete();
        }else{
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        }
        
    }

    private void executeHQLQueryDelete() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("DELETE User WHERE id_user = :idUser");
            q.setParameter("idUser", this.id_pegawai);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            executeHQLQuery();
            offClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/AdminHome.fxml"));
        pane.getChildren().setAll(panes);
    }

    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from User where id_toko = :idToko AND role = 2");
            q.setParameter("idToko", id_toko);
            List resultList = q.list();
            displayList(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayList(List ResultList) {
        tableData = FXCollections.observableArrayList();
        for (Object o : ResultList) {
            User user = (User) o;
            tableData.add(user);
        }
        TBLno.setCellValueFactory(new PropertyValueFactory<>("idUser"));
        TBLnama.setCellValueFactory(new PropertyValueFactory<>("nama"));
        TBLusername.setCellValueFactory(new PropertyValueFactory<>("username"));
        TBLpassword.setCellValueFactory(new PropertyValueFactory<>("password"));

        tabel.getColumns().clear();
        tabel.setItems(tableData);
        tabel.getColumns().addAll(TBLno, TBLnama, TBLusername, TBLpassword);

        tabel.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                onClick();
            }

        });
        tabel.setRowFactory(new Callback<TableView<User>, TableRow<User>>() {

            @Override
            public TableRow<User> call(TableView<User> param) {
                final TableRow<User> row = new TableRow<User>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < tabel.getItems().size() && tabel.getSelectionModel().isSelected(index)) {
                            tabel.getSelectionModel().clearSelection();
                            event.consume();
                            offClick();
                        }

                    }

                });
                return row;
            }

        });
    }

    public void onClick() {
        if (tabel.getSelectionModel().getSelectedItem() != null) {
            User forumSelect = tabel.getSelectionModel().getSelectedItem();
            this.id_pegawai = forumSelect.getIdUser();
            TFnama.setText(forumSelect.getNama());
            TFpassword.setText(forumSelect.getPassword());
            TFusername.setText(forumSelect.getUsername());
        }
    }

    public void offClick() {
        this.id_pegawai = -1;
        TFnama.setText("");
        TFpassword.setText("");
        TFusername.setText("");

    }

}
