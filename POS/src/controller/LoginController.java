/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class LoginController implements Initializable {
    @FXML
    private AnchorPane pane;
    @FXML
    private TextField TFusername;
    @FXML
    private PasswordField TFpassword;
    
    public static int id_toko;
    public String savedUsername;
    public String savedPassword;
    public static int id_user;
    public static int role;
    
    private static String QUERY_LOGIN = "From User us where us.username like '";
    
    /**
     * Initializes the controller class.
     */
    
     private void runLogin() {
        executeHQLQuery(QUERY_LOGIN + TFusername.getText().toString() + "'");
    }
    
    private void executeHQLQuery(String sql) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            for (Object o : resultList) {
                User user = (User) o;
                savedUsername = user.getUsername();
                savedPassword = user.getPassword();
                role = user.getRole();
                id_user = user.getIdUser();
                id_toko = user.getIdToko();
            }
            session.getTransaction().commit();
            if (savedPassword == null || savedUsername == null) {
                autentikasi(" ", " ");
            } else {
                autentikasi(savedPassword, savedUsername);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void autentikasi(String savedPassword, String savedUsername) throws IOException {
        if (savedPassword.equals(TFpassword.getText()) && savedUsername.equals(TFusername.getText())) {
            if(role == 1){
                AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/AdminHome.fxml"));
                pane.getChildren().setAll(panes);
            }else{
                AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Home.fxml"));
                pane.getChildren().setAll(panes);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Username atau password salah", ButtonType.OK);
            alert.setTitle("Username atau password salah");
            alert.showAndWait();
        }

    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void registerAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/Register.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    private void loginAction(ActionEvent event) {
        runLogin();
    }
    
}
