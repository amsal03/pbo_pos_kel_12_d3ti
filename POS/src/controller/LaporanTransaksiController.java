/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import hibernate.NewHibernateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Transaksi;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author AMSAL_SITUMORANG_11317035;
 */
public class LaporanTransaksiController implements Initializable {
    @FXML
    private AnchorPane pane;
    @FXML
    private TableView<Transaksi> tabel;
    @FXML
    private TableColumn BTNtotal;
    @FXML
    private TableColumn btnTgl;
    ObservableList<Transaksi> tableData = null;
    public int id_toko = LoginController.id_toko;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        executeHQLQuery();
    }    

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/AdminHome.fxml"));
        pane.getChildren().setAll(panes);
    }
    
    private void executeHQLQuery() {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Transaksi where id_toko = :idToko");
            q.setParameter("idToko", this.id_toko);
            List resultList = q.list();
            displayList(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void displayList(List ResultList) {
        tableData = FXCollections.observableArrayList();
        for (Object o : ResultList) {
            Transaksi tr = (Transaksi) o;
            tableData.add(tr);
        }
        BTNtotal.setCellValueFactory(new PropertyValueFactory<>("total"));
        btnTgl.setCellValueFactory(new PropertyValueFactory<>("tanggal"));
        
        tabel.getColumns().clear();
        tabel.setItems(tableData);
        tabel.getColumns().addAll(BTNtotal, btnTgl);

        
    }
    
}
