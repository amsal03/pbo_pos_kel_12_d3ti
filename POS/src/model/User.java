package model;
// Generated May 11, 2019 11:40:09 PM by Hibernate Tools 4.3.1



/**
 * User generated by hbm2java
 */
public class User  implements java.io.Serializable {


     private int idUser;
     private int idToko;
     private String username;
     private String password;
     private String nama;
     private int role;

    public User() {
    }

    public User(int idUser, int idToko, String username, String password, String nama, int role) {
       this.idUser = idUser;
       this.idToko = idToko;
       this.username = username;
       this.password = password;
       this.nama = nama;
       this.role = role;
    }
   
    public int getIdUser() {
        return this.idUser;
    }
    
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    public int getIdToko() {
        return this.idToko;
    }
    
    public void setIdToko(int idToko) {
        this.idToko = idToko;
    }
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public String getNama() {
        return this.nama;
    }
    
    public void setNama(String nama) {
        this.nama = nama;
    }
    public int getRole() {
        return this.role;
    }
    
    public void setRole(int role) {
        this.role = role;
    }




}


