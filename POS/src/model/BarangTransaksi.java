package model;
// Generated May 11, 2019 11:40:09 PM by Hibernate Tools 4.3.1



/**
 * BarangTransaksi generated by hbm2java
 */
public class BarangTransaksi  implements java.io.Serializable {


     private int idBarangTransaksi;
     private int idBarang;
     private int idTransaksi;
     private int jumlah;
     private int subTotal;
     

    public BarangTransaksi() {
    }

    public BarangTransaksi(int idBarangTransaksi, int idBarang, int idTransaksi, int jumlah, int subTotal) {
       this.idBarangTransaksi = idBarangTransaksi;
       this.idBarang = idBarang;
       this.idTransaksi = idTransaksi;
       this.jumlah = jumlah;
       this.subTotal = subTotal;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }
   
    public int getIdBarangTransaksi() {
        return this.idBarangTransaksi;
    }
    
    public void setIdBarangTransaksi(int idBarangTransaksi) {
        this.idBarangTransaksi = idBarangTransaksi;
    }
    public int getIdBarang() {
        return this.idBarang;
    }
    
    public void setIdBarang(int idBarang) {
        this.idBarang = idBarang;
    }
    public int getIdTransaksi() {
        return this.idTransaksi;
    }
    
    public void setIdTransaksi(int idTransaksi) {
        this.idTransaksi = idTransaksi;
    }
    public int getJumlah() {
        return this.jumlah;
    }
    
    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }




}


